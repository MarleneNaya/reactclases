import React, { Component } from 'react';
import { mongo } from '../index';

export class DatosExtras extends Component {
  state={
    wineries: [],
    wines: [],
    country: '',
    variety: '',
    winery: '',
    price: '',
  }
  vinos= mongo.db('app').collection('vinos')
  componentDidMount(){
    this.vinos.count()
    this.BodegasPrecioPuntos();

  }
  // Pedir las diez bodegas mas caras y con mas puntos y arreglarlas por orden de puntos
  BodegasPrecioPuntos= () => {
    let query= [
    { $group:  { _id: '$winery', total: { $sum: 1 } , priceAvg: {$avg: '$price'}, pointsAvg: {$av: '$points'}   } },
    { $limit: 10 },
    { $sort: { _id: - 1} }]
    
    this.vinos.aggregate(query).toArray()
    .then(d=> {
      console.log(d)
      this.setState({ wineries: d})
    })
  }

  render() {
    return (
      <div>
        {JSON.stringify(this.state.wineries)}
       
        
      </div>
    )
  }
}
