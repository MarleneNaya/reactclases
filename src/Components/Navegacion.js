import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';




export class Navegacion extends Component {


  render() {


    return (

      <div>

        <Navbar expand="lg" variant="light" bg="dark" className="bg-dark justify-content-between">

          <Navbar.Brand href="/home" style={{ fontSize: "25px", fontFamily: "palatino" }}>
            <i className="material-icons" style={{ fontSize: "38px" }} > house</i>
            &nbsp; Selecciona el mejor vino por... &nbsp; </Navbar.Brand>
          <Navbar.Brand href="/vinos" style={{ fontSize: "25px", fontFamily: "palatino" }}>País</Navbar.Brand>
          <Navbar.Brand href="/precio" style={{ fontSize: "25px", fontFamily: "palatino" }}>Precio</Navbar.Brand>
          <Navbar.Brand href="/puntos" style={{ fontSize: "25px", fontFamily: "palatino" }}>Puntos</Navbar.Brand>
          <Navbar.Brand href="/DatosExtras" style={{ fontSize: "25px", fontFamily: "palatino" }} >Datos Extras</Navbar.Brand>
          <Navbar.Brand href="/login" style={{ fontSize: "25px", fontFamily: "palatino" }} >Login </Navbar.Brand>
        </Navbar>

      </div>
    )
  }
}
